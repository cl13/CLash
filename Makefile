OUT = /usr/bin/CLash


build:
	cd src/ && make

install: build
	sudo cp ./src/build/CLash $(OUT)

uninstall:
	sudo rm -r $(OUT)
