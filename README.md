# CLash
## A Shell for Unix
<p align="center">
	<img width="400" alt="CLash Logo" src="Assets/CLash.png">
</p>

# Requirements
``readline``

# Install
To install look at <a href="https://gitlab.com/cl13/CLash/-/blob/master/INSTALL">INSTALL</a>.

Or:

``git clone https://gitlab.com/cl13/CLash.git``

``cd CLash``

``make install``

# What is it?
This Shell is a shell made for the <a href="https://gitlab.com/cl13/CLTerm/">CLTerm</a>.
But you can still use it at your own risk as your daily shell.
