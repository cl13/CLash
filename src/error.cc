#include "error.hh"
#include <iostream>

void err(char* err_var, char color[8])
{
	std::cerr << color << "Couldn't execute: " << err_var << std::endl;
}

void err(char** err_var, char color[8])
{
	std::cerr << color << "Couldn't execute: " << err_var << std::endl;
}

