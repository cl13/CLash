#include <iostream>
#include <unistd.h>
#include <readline/readline.h>
#include <sys/wait.h>
#include "shell.hh"
#include "colors.hh"
#include "error.hh"

Colors color
{
	{0x1b, '[', '1', ';', '3', '4', 'm', 0},                      // blue
	{0x1b, '[', '1', ';', '3', '5', 'm', 0},                      // purple
	{0x1b, '[', '1', ';', '3', '2', 'm', 0},                      // green
	{0x1b, '[', '1', ';', '3', '1', 'm', 0},                      // red
	{0x1b, '[', '1', ';', '9', '7', 'm', 0},                      // white
	{0x1b, '[', 'K', 0},                                          // expand bg
	{0x1b, '[', '0', ';', '1', '0', '4', 'm', 0x1b, '[', 'K', 0}, // blue bg
	{0x1b, '[', '0', ';', '1', '0', '1', 'm', 0x1b, '[', 'K', 0}, // red bg
	{0x1b, '[', '0', ';', '1', '0', '2', 'm', 0x1b, '[', 'K', 0}, // green bg
	{0x1b, '[', '1', 'm', 0},                                     // bold
	{0x1b, '[', '4', 'm', 0},                                     // underline
	{0x1b, '[', '0', 'm', 0}                                      // reset
};

int main(int argc, char* argv[])
{
    char** command;
    char* input;
    pid_t child_pid;
    int stat_loc;
	clear;

	bool loop = true;
	do
    {
    	PS1(color.blue, color.purple, color.white);
        input = readline("");
        command = getInput(input);

        child_pid = fork();
        if (child_pid < 0)
        {
            std::cerr << color.red << "Fork failed" << std::endl;
            exit(1);
        }
        if (child_pid == 0)
        {
            execvp(command[0], command);
			std::cerr << color.red << "Couldn't execute: " << input << std::endl;
        }
        if (strcmp(command[0], "cd") == 0)
        {
        	if (cd(command[1]) < 0)
                std::cerr << color.red << "Couldn't execute: " << command[1] << std::endl;
            continue;
        }
        else 
            waitpid(child_pid, &stat_loc, WUNTRACED);
        

        free(input);
        free(command);
        
    } while(loop == true);

    return 0;
}
