#ifndef __COLORS__HH__CLASH__
#define __COLORS__HH__CLASH__

typedef struct colors
{
	char blue[8];
	char purple[8];
	char green[8];
	char red[8];
	char white[8];
	char expand_bg[4];
	char blue_bg[13];
	char red_bg[13];
	char green_bg[13];
	char bold[5];
	char underline[5];
	char reset[5];

} Colors;

#define clear printf("\033[H\033[J")

#endif
