#include "shell.hh"
#include <iostream>
#include <sstream>
#include <limits.h>
#include <unistd.h>
#include <string.h>

void PS1(char color1[8], char color2[8], char color3[8])
{        
    char hostname[HOST_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);

	std::stringstream ps1;
	ps1 << color1 << std::getenv("USER") << "@" << color2
	 << hostname << " " << color3;
    
    std::cout << ps1.str().c_str();
}

void PS_backslash(char color1[8], char color2[8])
{
    std::cout << color1 << "> " << color2;
}

char **getInput(char *input)
{
    char** command = (char**) malloc(8 * sizeof(char*));
    char* separator = " ";
    char* parsed;
    int index = 0;

    parsed = strtok(input, separator);
    while (parsed != NULL) {
        command[index] = parsed;
        index++;

        parsed = strtok(NULL, separator);
    }

    command[index] = NULL;
    return command;
}

int cd(char *path)
{
    return chdir(path);
}
