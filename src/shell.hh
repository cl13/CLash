#ifndef __SHELL_HH__
#define __SHELL_HH__

void PS1(char[8], char[8], char[8]);
void PS_backslash(char[8], char[8]);
char** getInput(char*);
int cd(char *path);

#endif
